<?php
    class Htmalpage {
        protected $title = "its a title";
        protected $body = "its a body";
        public function view() {
            echo '<br>';
            echo "<html>
                <head>
                  <title>$this->title</title>
                </head>
                <body>
                    $this->body
                </body>
                </html>";
        }
        function __construct($title = "", $body = ""){
            if($title != ""){
                $this->title = $title;
            }
            if($body != ""){
                $this->body = $body;
            }
        }
    }
    class coloredText extends Htmalpage{
        protected $color = 'red';
        protected $colors = array('red','yellow','green');
        public function __set($property,$value){
            if ($property == 'color'){
                if (in_array($value,$this->colors)){
                    $this->color = $value;
                }
                else{
                    $this->color = 'black';
                    $this->body = 'change color';
                } 
            }
        }
        public function view() {
            echo "<html>
                <head>
                  <title>$this->title</title>
                </head>
                <body>
                    <p style = 'color:$this->color'>$this->body</p>
                </body>
                </html>";
        }
    }
    class sizeText extends coloredText{
        protected $size = 20;
        public function __set($property,$value){
            if ($property == 'color'){
                parent::__set($property,$value);
            }
            elseif ($property == 'size'){
                if ($value >= 10 && $value <= 24){
                    $this->size = $value;
                }
                else{
                    $this->size = 14;
                    $this->body = 'change size';
                } 
            }
        }
        public function view() {
            echo "<html>
            <head>
            <title>$this->title</title>
            </head>
            <body>
                <p style = 'font-size:$this->size;color:$this->color'>$this->body</p> 
            </body>
            </html>";
        }
    }
?>